package tests.bdd.steps

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import org.bouncycastle.cms.RecipientId.password
import org.testng.Assert
import page.mailru.InboxPage

class LoginSteps : BaseSteps() {
    private var inboxPage: InboxPage? = null

    @Given("I go to mail.ru page")
    fun iGoToMailRuPage() {
        mailRuPage.openUrl()
    }

    @And("I sign in as {username} with password {password}")
    fun iSignInAsWithPassword(username: String, password: String) {
        inboxPage = mailRuPage.signInWithCredentials(username, password)
    }

    @Then("I should see inbox page")
    fun iShouldSeeInboxPage() {
        Assert.assertTrue(inboxPage!!.isItInboxPageTitle)
    }
}
