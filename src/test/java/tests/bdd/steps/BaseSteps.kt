package tests.bdd.steps

import decorator.WebDriverDecorator
import factory.BrowserManager
import factory.BrowserManagerFactory
import page.mailru.MailRuPage

import util.propertiesreader.PropertiesReader.getPropertyValue

open class BaseSteps {
    protected var webDriverDecorator: WebDriverDecorator
    protected var mailRuPage: MailRuPage

    init {
        BrowserManagerFactory.getManager(getPropertyValue("browser")).createDriver()
        webDriverDecorator = WebDriverDecorator(BrowserManager.driver)
        mailRuPage = MailRuPage(webDriverDecorator)
    }

    companion object {
        val testUserLogin: String = getPropertyValue("username")
        val testUserPassword: String = getPropertyValue("password")
    }
}