package tests

import org.testng.Assert
import org.testng.annotations.Test
import page.mailru.EMailPage

class FMailTest : BasicMailSetUp() {
    @Test
    fun testMailInFluentStyle() {
        mailRuPage
            .openUrl()
            .signInWithCredentials(user.userName, user.password)
            .writeNewLetter()
            .writeLetter(letter.getter, letter.subject, letter.text)
            .saveAsDraft()
            .sendLetterFromDrafts()
            .logOut()
        val eMailPage = EMailPage(driver)
        Assert.assertTrue(eMailPage.isLogOutSuccessful)
    }
}
