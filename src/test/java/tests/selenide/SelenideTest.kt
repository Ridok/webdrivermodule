package tests.selenide

import org.openqa.selenium.By
import org.testng.annotations.Test
import tests.BasicMailSetUp

import com.codeborne.selenide.Selenide.*

class SelenideTest : BasicMailSetUp() {
    @Test
    fun selenideMailTest() {
        open("https://www.mail.ru")
        `$`(By.id("mailbox:login")).sendKeys(user.userName)
        `$`(By.id("mailbox:password")).sendKeys(user.password)
        `$`(By.id("mailbox:submit")).click()
        `$$`(By.id("//*[@id=\"mailbox:error\"]")).shouldHaveSize(0)
    }
}
