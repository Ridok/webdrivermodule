package tests

import decorator.WebDriverDecorator
import factory.BrowserManager
import org.testng.annotations.BeforeClass
import page.mailru.MailRuPage
import util.entities.Letter
import util.entities.User
import util.propertiesreader.PropertiesReader.getPropertyValue

open class BasicMailSetUp {
    protected lateinit var mailRuPage: MailRuPage
    protected lateinit var user: User
    protected lateinit var letter: Letter
    protected lateinit var driver: WebDriverDecorator

    @BeforeClass
    fun setUp() {
        driver = BrowserManager.driver
        driver.manage().window().maximize()
        mailRuPage = MailRuPage(driver)
        user = User(getPropertyValue("username"), getPropertyValue("password"))
        letter = Letter(getPropertyValue("getter"), getPropertyValue("subject"), getPropertyValue("text"))
    }
}
