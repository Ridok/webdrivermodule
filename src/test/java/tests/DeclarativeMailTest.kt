package tests

import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import org.testng.asserts.SoftAssert
import page.mailru.DraftsPage
import page.mailru.InboxPage
import page.mailru.NewLetterPage
import page.mailru.SentPage

@Test(singleThreaded = true)
class DeclarativeMailTest : BasicMailSetUp() {
    private lateinit var inboxPage: InboxPage
    private lateinit var newLetterPage: NewLetterPage
    private lateinit var draftsPage: DraftsPage
    private lateinit var sentPage: SentPage
    private lateinit var softAssert: SoftAssert

    @BeforeClass
    fun setUpSoftAssert() {
        softAssert = SoftAssert()
    }

    @Test
    fun testLogIn() {
        mailRuPage.openUrl()
        inboxPage = mailRuPage.signInWithCredentials(user.userName, user.password)
        softAssert.assertTrue(inboxPage.isLoginSuccessful, "Log in isn't successful.")
        softAssert.assertTrue(inboxPage.isItInboxPageTitle, "Title isn't correct.")
        softAssert.assertTrue(inboxPage.isFooterOnPage, "There is no footer.")
        softAssert.assertAll()
    }

    @Test(dependsOnMethods = ["testLogIn"])
    fun testDraftLetter() {
        newLetterPage = inboxPage.writeNewLetter()
        newLetterPage.writeLetter(letter.getter, letter.subject, letter.text)
        draftsPage = newLetterPage.saveAsDraft()
        softAssert.assertTrue(draftsPage.isLetterInDrafts, "Letter isn't in drafts.")
        softAssert.assertEquals(draftsPage.getDraftGetter(), letter.getter, "Getter isn't valid.")
        softAssert.assertTrue(draftsPage.getDraftSubject()!!.contains(letter.subject), "Subject isn't valid.")
        softAssert.assertTrue(draftsPage.getDraftMessage()!!.contains(letter.text), "Message isn't valid.")
        softAssert.assertAll()
    }

    @Test(dependsOnMethods = ["testDraftLetter"])
    fun testSendLetter() {
        sentPage = draftsPage.sendLetterFromDrafts()
        softAssert.assertTrue(sentPage.isLetterInSentFolder)
        softAssert.assertEquals(sentPage.sentGetter, letter.getter, "Getter isn't valid.")
        softAssert.assertTrue(sentPage.getSentSubject()!!.contains(letter.subject), "Subject isn't valid.")
        softAssert.assertTrue(sentPage.getSentMessage()!!.contains(letter.text), "Message isn't valid.")
        softAssert.assertAll()
    }

    @Test(dependsOnMethods = ["testSendLetter"])
    fun testLogOut() {
        sentPage.logOut()
        softAssert.assertTrue(sentPage.isLogOutSuccessful, "Log out isn't successful.")
        softAssert.assertAll()
    }
}
