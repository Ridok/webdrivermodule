package factory

import decorator.WebDriverDecorator
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class FirefoxBrowserManager : BrowserManager() {
    private val log: Logger = LoggerFactory.getLogger(FirefoxBrowserManager::class.java)

    override fun createDriver() {
        BrowserManager.driverThreadLocal.set(WebDriverDecorator(FirefoxDriver(CapabilityFactory.getCapabilities("FIREFOX") as FirefoxOptions)))
        log.info("ChromeDriver was created")
    }

    companion object {
        private var instance: FirefoxBrowserManager? = null

        fun getInstance(): BrowserManager {
            if (instance == null) {
                instance = FirefoxBrowserManager()
            }
            return instance as FirefoxBrowserManager
        }
    }
}
