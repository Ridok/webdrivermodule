package factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxOptions

internal object CapabilityFactory {
    fun getCapabilities(name: String): Capabilities {
        return when (name) {
            "CHROME" -> ChromeOptions()
            "FIREFOX" -> FirefoxOptions()
            else -> FirefoxOptions()
        }
    }
}
