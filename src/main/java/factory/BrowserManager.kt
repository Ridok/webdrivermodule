package factory

import decorator.WebDriverDecorator
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.MalformedURLException
import java.net.URL

abstract class BrowserManager {

    abstract fun createDriver()

    companion object {
        private val log: Logger = LoggerFactory.getLogger(BrowserManager::class.java)
        internal var driverThreadLocal: ThreadLocal<WebDriverDecorator> = ThreadLocal()

        fun quitDriver() {
            if (driverThreadLocal.get() != null) {
                driverThreadLocal.get().quit()
            }
        }

        fun setUpRemoteWebDriver() {
            var url: URL? = null
            try {
                url = URL("http://localhost:4444/wd/hub")
            } catch (e: MalformedURLException) {
                log.debug(e.message)
            }

            val remoteWebDriver = RemoteWebDriver(url, DesiredCapabilities.chrome())
            driverThreadLocal.set(WebDriverDecorator(remoteWebDriver))
        }

        val driver: WebDriverDecorator
            get() = driverThreadLocal.get()
    }
}
