package factory

import decorator.WebDriverDecorator
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class ChromeBrowserManager : BrowserManager() {
    private val log: Logger = LoggerFactory.getLogger(ChromeBrowserManager::class.java)

    override fun createDriver() {
        BrowserManager.driverThreadLocal.set(WebDriverDecorator(ChromeDriver(CapabilityFactory.getCapabilities("CHROME") as ChromeOptions)))
        log.info("ChromeDriver was created")
    }

    companion object {
        private var instance: ChromeBrowserManager? = null

        fun getInstance(): BrowserManager {
            if (instance == null) {
                instance = ChromeBrowserManager()
            }
            return instance as ChromeBrowserManager
        }
    }
}