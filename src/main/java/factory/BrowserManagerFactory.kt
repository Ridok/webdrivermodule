package factory

object BrowserManagerFactory {
    fun getManager(name: String): BrowserManager {
        return when (name) {
            "CHROME" -> ChromeBrowserManager.getInstance()
            "FIREFOX" -> FirefoxBrowserManager.getInstance()
            else -> FirefoxBrowserManager.getInstance()
        }
    }
}
