package decorator

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.FluentWait
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import util.constant.IMPLICIT_WAIT
import java.time.Duration

class WebDriverDecorator(private val driver: WebDriver) : WebDriver {
    private val log: Logger = LoggerFactory.getLogger(WebDriverDecorator::class.java)

    override fun get(url: String) {
        driver.get(url)
    }

    override fun getCurrentUrl(): String {
        return driver.currentUrl
    }

    override fun getTitle(): String {
        return driver.title
    }

    override fun findElements(by: By): List<WebElement> {
        return driver.findElements(by)
    }

    override fun findElement(by: By): WebElement {
        return driver.findElement(by)
    }

    override fun getPageSource(): String {
        return driver.pageSource
    }

    override fun close() {
        driver.close()
    }

    override fun quit() {
        driver.quit()
    }

    override fun getWindowHandles(): Set<String> {
        return driver.windowHandles
    }

    override fun getWindowHandle(): String {
        return driver.windowHandle
    }

    override fun switchTo(): WebDriver.TargetLocator {
        return driver.switchTo()
    }

    override fun navigate(): WebDriver.Navigation {
        return driver.navigate()
    }

    override fun manage(): WebDriver.Options {
        return driver.manage()
    }

    fun waitForElementPresent(locator: By, timeout: Long = IMPLICIT_WAIT.toLong()) {
        FluentWait(driver)
            .withTimeout(Duration.ofMillis(timeout))
            .pollingEvery(Duration.ofSeconds(1))
            .ignoring(TimeoutException::class.java)
            .until { webDriver -> webDriver.findElements(locator).isNotEmpty() }
    }

    fun waitForElementPresent(element: WebElement?, timeout: Long = IMPLICIT_WAIT.toLong()) {
        FluentWait(driver)
            .withTimeout(Duration.ofMillis(timeout))
            .pollingEvery(Duration.ofSeconds(1))
            .ignoring(TimeoutException::class.java)
            .until { element!!.isDisplayed }
    }

    fun waitForAttributeValuePresent(webElement: WebElement?, attributeName: String) {
        FluentWait(webElement)
            .withTimeout(Duration.ofSeconds(60))
            .pollingEvery(Duration.ofMillis(500))
            .ignoring(TimeoutException::class.java)
            .until<Boolean> { element -> element.getAttribute(attributeName).isNotEmpty() }
    }

    fun waitForPageLoad() {
        FluentWait(driver)
            .withTimeout(Duration.ofSeconds(60))
            .pollingEvery(Duration.ofMillis(500))
            .until { webDriver -> (webDriver as JavascriptExecutor).executeScript("return document.readyState") == "complete" }
    }

    fun clickStale(by: By) {
        var staleElement = true
        while (staleElement) {
            try {
                driver.findElement(by).click()
                staleElement = false
            } catch (ignored: StaleElementReferenceException) {
                log.debug(ignored.message)
            }

        }
    }

    fun clickStale(element: WebElement?) {
        var staleElement = true
        while (staleElement) {
            try {
                element!!.click()
                staleElement = false
            } catch (ignored: StaleElementReferenceException) {
                log.debug(ignored.message)
            }

        }
    }

    fun acceptAlertIfPresent() {
        try {
            driver.switchTo().alert().accept()
        } catch (e: NoAlertPresentException) {
            log.info("No alert present.")
        }

    }
}
