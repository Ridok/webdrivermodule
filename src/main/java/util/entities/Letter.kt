package util.entities

class Letter(
    val getter: String,
    val subject: String,
    val text: String
)
