package util.propertiesreader

import java.io.IOException
import java.io.InputStream
import java.util.Properties
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object PropertiesReader {
    private val log: Logger = LoggerFactory.getLogger(PropertiesReader.javaClass)
    private const val PROPERTIES_FILE_NAME = "test.properties"
    private val property = Properties()

    fun getPropertyValue(propertyName: String): String {
        loadProperties()
        return property.getProperty(propertyName)
    }

    private fun loadProperties() {
        try {
            val inputStream = getInputStreamFromResources()
            property.load(inputStream)
        } catch (e: IOException) {
            log.error(e.message)
        }
    }

    private fun getInputStreamFromResources(): InputStream {
        val classLoader = javaClass.classLoader
        val resource = classLoader.getResource(PROPERTIES_FILE_NAME)
        return if (resource == null) {
            throw IllegalArgumentException("File is not found!")
        } else {
            resource.openStream()
        }
    }
}
