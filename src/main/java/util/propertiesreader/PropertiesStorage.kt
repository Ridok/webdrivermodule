package util.propertiesreader

object PropertiesStorage {
    private const val USER_DIRECTORY = "user.dir"
    private const val FILE_SEPARATOR = "file.separator"

    val userDirectory: String
        get() = System.getProperty(USER_DIRECTORY)

    val fileSeparator: String
        get() = System.getProperty(FILE_SEPARATOR)
}
