package util.listener

import org.apache.commons.lang3.exception.ExceptionUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.IConfigurationListener
import org.testng.ITestResult


class ConfigListener : IConfigurationListener {
    private val log: Logger = LoggerFactory.getLogger(ConfigListener::class.java)

    override fun onConfigurationSuccess(itr: ITestResult) {
        log.info("{} {} SUCCESS ===========", CONFIG_MESSAGE_START, itr.name)
    }

    override fun onConfigurationFailure(itr: ITestResult) {
        val throwable = itr.throwable
        val throwableMsg: String
        throwableMsg = if (throwable.message == null) {
            "(Message not defined)"
        } else {
            throwable.message!!
        }
        log.info(ExceptionUtils.getStackTrace(throwable))
        if (throwable is AssertionError) {
            log.info("Assertion failure: {}", throwableMsg)
        } else {
            log.warn("Error: {} - {}", throwable.javaClass.simpleName, throwableMsg)
        }
        log.info("{} {} FAILED ===========", CONFIG_MESSAGE_START, itr.name)
    }

    override fun onConfigurationSkip(itr: ITestResult) {
        val throwable = itr.throwable
        if (throwable != null) {
            log.warn(throwable.javaClass.simpleName + ": " + throwable.message)
            log.info("{} {} SKIPPED ===========", CONFIG_MESSAGE_START, itr.name)
        }
    }

    companion object {
        private const val CONFIG_MESSAGE_START = "=========== CONFIG"
    }
}
