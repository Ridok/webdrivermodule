package util.listener

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.IAlterSuiteListener
import org.testng.xml.XmlSuite

class AlterSuiteListener : IAlterSuiteListener {
    private val log: Logger = LoggerFactory.getLogger(AlterSuiteListener::class.java)
    /** Can be used to modify suites. only this listener allows final
     * modifications for suites that later will be passed in modified state
     * for execution (particularly to SuiteListener)  */
    override fun alter(suites: List<XmlSuite>) {
        log.info("Passed suites number: " + suites.size)
        suites.forEach { s -> log.info("Suites: " + s.name) }
        suites.forEach { s -> s.tests.forEach { t -> log.info("Tests: " + t.name) } }
    }
}
