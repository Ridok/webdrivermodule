package util.listener

import factory.BrowserManager
import factory.BrowserManagerFactory
import org.apache.commons.io.FileUtils
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.ITestContext
import org.testng.ITestListener
import org.testng.ITestNGMethod
import org.testng.ITestResult
import org.testng.TestRunner
import util.constant.REPORT_TIME_FORMATTER
import util.constant.TEST_OUTPUT_FOLDER
import util.propertiesreader.PropertiesReader
import util.propertiesreader.PropertiesStorage
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.time.LocalDate
import java.time.LocalTime
import java.util.Arrays
import java.util.function.Function
import java.util.stream.Collectors

class TestListener : ITestListener {
    private val log: Logger = LoggerFactory.getLogger(TestListener::class.java)
    private var outputDir: String? = null

    override fun onTestFailure(result: ITestResult) {
        log.info(TEST_MESSAGE_START + result.name + " FAILED ========")
        val throwable = result.throwable
        val throwableMsg: String
        if (throwable.message == null) {
            throwableMsg = " (Message not defined)"
        } else {
            throwableMsg = " - " + throwable.message
        }
        if (throwable is AssertionError) {
            log.info("Assertion failure: $throwableMsg")
        } else {
            log.warn("Error: " + throwable.javaClass.getSimpleName() + throwableMsg)
        }
        val src = (BrowserManager.driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
        try {
            FileUtils.copyFile(src, File(outputDir + PropertiesStorage.fileSeparator + "screenshot.png"))
        } catch (e: IOException) {
            log.debug("Failed to save screenShot.")
        }

    }

    override fun onTestStart(result: ITestResult) {
        log.info(TEST_MESSAGE_START + result.name + " STARTED ========")
    }

    override fun onTestSuccess(result: ITestResult) {
        log.info(TEST_MESSAGE_START + result.name + " PASSED ========")
    }

    override fun onTestSkipped(result: ITestResult) {
        log.info(TEST_MESSAGE_START + result.name + " SKIPPED ========")
        val throwable = result.throwable
        if (throwable != null) {
            log.warn(throwable.javaClass.getSimpleName() + ": " + throwable.message)
        }
    }

    override fun onTestFailedButWithinSuccessPercentage(result: ITestResult) {
        log.info(TEST_MESSAGE_START + result.name + " FAILED (success percentage) ========")
    }

    override fun onStart(context: ITestContext) {
        log.info(XML_TEST_MESSAGE_START + context.name + " STARTED ==============")
        log.info("Tests methods found: " + Arrays.stream(context.allTestMethods)
            .map<String> { it.methodName }
            .collect<String, Any>(Collectors.joining(",")))
        outputDir = TEST_OUTPUT_FOLDER + PropertiesStorage.fileSeparator + LocalDate.now() +
            PropertiesStorage.fileSeparator + LocalTime.now().format(REPORT_TIME_FORMATTER)
        setOutputDir(outputDir, context as TestRunner)
        if (PropertiesReader.getPropertyValue("grid") == "false") {
            BrowserManagerFactory.getManager(PropertiesReader.getPropertyValue("browser")).createDriver()
        } else {
            BrowserManager.setUpRemoteWebDriver()
        }
    }

    override fun onFinish(context: ITestContext) {
        log.info(XML_TEST_MESSAGE_START + context.name + " FINISHED ==============")
        BrowserManager.quitDriver()
        val resultsStr = ResultStorage.instance.getResults().stream().map({ suite ->
            val stringBuilder = StringBuilder()
            suite.getResults().forEach({ name, result ->
                stringBuilder.append("Test name: ").append(name).append("\n")
                    .append("Failed tests: ").append(result.getTestContext().getFailedTests().size()).append("\n")
                    .append("Skipped tests: ").append(result.getTestContext().getSkippedTests().size()).append("\n")
                    .append("Passed tests: ").append(result.getTestContext().getPassedTests().size())
            })
            String.format("Suite name: %s\n results: %s", suite.getName(), stringBuilder.toString())
        }).collect(Collectors.joining("\n"))
        val report = File(outputDir + PropertiesStorage.fileSeparator + "report.txt")
        val writer: FileWriter
        try {
            writer = FileWriter(report)
            writer.append(resultsStr)
            writer.close()
        } catch (e: IOException) {
            log.debug(e.message)
        }

    }

    private fun setOutputDir(outputDir: String, testRunner: TestRunner) {
        val dir = File(outputDir)
        if (!dir.exists()) {
            val wasSuccessful = dir.mkdirs()
            log.info("Directories created successfully: $wasSuccessful")
        }
        testRunner.outputDirectory = outputDir
    }

    companion object {
        private val TEST_MESSAGE_START = "======== TEST METHOD "
        private val XML_TEST_MESSAGE_START = "============== XML TEST "
    }
}
