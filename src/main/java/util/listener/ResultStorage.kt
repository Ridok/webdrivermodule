package util.listener

import org.testng.ISuite
import java.util.ArrayList
import java.util.Collections
import java.util.HashSet

class ResultStorage private constructor() {
    private val results: MutableList<ISuite> = Collections.synchronizedList(ArrayList())

    fun addResults(suite: ISuite) {
        results.add(suite)
    }

    fun withoutDuplicates(): List<ISuite> {
        val list = results
        val hashSet = HashSet<ISuite>(list)
        list.clear()
        list.addAll(hashSet)
        return list
    }

    companion object {
        private val INSTANCES = ThreadLocal.withInitial { ResultStorage() }

        val instance: ResultStorage
            get() = INSTANCES.get()
    }
}
