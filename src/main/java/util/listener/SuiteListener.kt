package util.listener

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.ISuite
import org.testng.ISuiteListener

class SuiteListener : ISuiteListener {
    private val log: Logger = LoggerFactory.getLogger(SuiteListener::class.java)

    override fun onStart(suite: ISuite) {
        log.info(SUITE_MESSAGE_START + suite.name + " STARTED =================")
    }

    override fun onFinish(suite: ISuite) {
        log.info(SUITE_MESSAGE_START + suite.name + " FINISHED =================")
        ResultStorage.instance.addResults(suite)
    }

    companion object {
        private val SUITE_MESSAGE_START = "================= SUITE "
    }
}
