package util.constant

import java.time.format.DateTimeFormatter

object Xpathes {
    const val DRAFT_GETTER = "%s//div[@class='b-datalist__item__addr']"
    const val DRAFT_SUBJECT = "%s//div[@class='b-datalist__item__subj']"
    const val DRAFT_MESSAGE = "%s//span[@class='b-datalist__item__subj__snippet']"
}

const val IMPLICIT_WAIT = 10000
const val CUSTOM_URL = "https://www.epam.com"
const val MAIL_RU_URL = "https://www.mail.ru"
const val MAIL_RU_TITLE = "Входящие - Почта Mail.Ru"
val REPORT_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH-mm-ss-SSS")!!
const val TEST_OUTPUT_FOLDER = "test-output"

