package page

import decorator.WebDriverDecorator
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.support.ui.FluentWait
import page.Urls.CUSTOM
import java.time.Duration

open class CustomPage(var driver: WebDriverDecorator) {
    var url: String = CUSTOM.url

    val currentUrl: String
        get() = driver.currentUrl

    open fun openUrl(): CustomPage {
        driver.get(url)
        return this
    }

    fun <T : CustomPage> waitForCondition(function: (T) -> Boolean, page: T) {
        FluentWait(page)
            .withTimeout(Duration.ofSeconds(30))
            .pollingEvery(Duration.ofMillis(500))
            .ignoring(TimeoutException::class.java)
            .until(function)
    }
}
