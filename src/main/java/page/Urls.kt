package page

import util.constant.CUSTOM_URL
import util.constant.MAIL_RU_URL

enum class Urls private constructor(val url: String) {
    DRAFT("https://e.mail.ru/messages/drafts/"),
    CUSTOM(CUSTOM_URL),
    EMAIL("https://e.mail.ru/messages/"),
    INBOX("https://e.mail.ru/messages/inbox/"),
    MAIL_RU(MAIL_RU_URL),
    SENT("https://e.mail.ru/messages/sent/")
}
