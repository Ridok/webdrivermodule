package page.mailru

import decorator.WebDriverDecorator
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class NewLetterPage(driver: WebDriverDecorator) : EMailPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(NewLetterPage::class.java)
    @FindBy(xpath = "//input[@name='draft_msg']")
    private val draftLetterInfo: WebElement? = null

    @FindBy(xpath = "//*[@id=\"b-toolbar__right\"]/div/div/div[2]/div[2]/div/div[3]/div[2]/a[1]")
    private val saveAsDraft: WebElement? = null

    @FindBy(xpath = "//form[@action='/compose/']")
    private val editField: WebElement? = null

    @FindBy(id = "tinymce")
    private val letterFiled: WebElement? = null

    @FindBy(xpath = "//textarea[@data-original-name='To']")
    private val newLetterGetterField: WebElement? = null

    @FindBy(xpath = "//input[@name='Subject']")
    private val newLetterTopicField: WebElement? = null

    @FindBy(xpath = "//div[@data-group='save-more']")
    private val saveMoreButton: WebElement? = null

    private val idOfLetterInDrafts: DraftsPage
        get() {
            driver.waitForAttributeValuePresent(draftLetterInfo, "value")
            val draft = DraftsPage(driver)
            draft.setId(draftLetterInfo!!.getAttribute("value"))
            return draft
        }

    init {
        PageFactory.initElements(this.driver, this)
    }

    fun writeLetter(getter: String, topic: String, text: String): NewLetterPage {
        Actions(driver).sendKeys(newLetterGetterField!!, getter)
            .sendKeys(newLetterTopicField!!, topic)
            .build()
            .perform()
        writeText(text)
        return NewLetterPage(driver)
    }

    fun saveAsDraft(): DraftsPage {
        log.info("Saving letter as draft")
        saveMoreButton!!.click()
        saveAsDraft!!.click()
        val draft = idOfLetterInDrafts
        return moveToDrafts(draft)
    }

    internal fun setUrl(url: String) {
        this.url = url
    }

    private fun writeText(text: String) {
        val idEditor = editField!!.getAttribute("id").substring(8, 23)
        val editFrame = driver.findElement(By.xpath("//iframe[@id='toolkit-" + idEditor + "composeEditor_ifr']"))
        driver.switchTo().frame(editFrame)
        letterFiled!!.click()
        Actions(driver)
            .click(letterFiled)
            .sendKeys(letterFiled, text)
            .build()
            .perform()
        driver.switchTo().parentFrame()
    }

    private fun moveToDrafts(draft: DraftsPage): DraftsPage {
        pressDraftsButton()
        driver.acceptAlertIfPresent()
        waitForCondition(DraftsPage::isLetterInDrafts, draft)
        return draft
    }
}
