package page.mailru

import decorator.WebDriverDecorator
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import page.Urls
import util.constant.Xpathes.DRAFT_GETTER
import util.constant.Xpathes.DRAFT_MESSAGE
import util.constant.Xpathes.DRAFT_SUBJECT

class DraftsPage(driver: WebDriverDecorator) : EMailPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(DraftsPage::class.java)
    @FindBy(xpath = "//a[text()='письмо']")
    private val letterHyperlink: WebElement? = null
    @FindBy(xpath = "//*[@id=\"b-letter\"]/div[2]")
    private val letterId: WebElement? = null
    @FindBy(xpath = "//*[@id=\"b-toolbar__right\"]/div[3]/div/div[2]/div[1]/div")
    private val sendButton: WebElement? = null

    private var draftGetter: String? = null
    private var draftSubject: String? = null
    private var draftMessage: String? = null
    private var xpathOnNewLetter: String? = null

    val isLetterInDrafts: Boolean
        get() {
            log.info("Checking is letter in drafts folder")
            return !driver.findElements(By.xpath(xpathOnNewLetter!!)).isEmpty()
        }

    private val idOfSentLetter: String
        get() {
            driver.waitForElementPresent(letterHyperlink)
            letterHyperlink!!.click()
            driver.waitForElementPresent(letterId)
            return letterId!!.getAttribute("data-letter-id")
        }

    init {
        this.url = Urls.DRAFT.url
        PageFactory.initElements(this.driver, this)
    }

    private fun getLetterInfoInDrafts() {
        this.draftGetter = driver.findElement(By.xpath(String.format(DRAFT_GETTER, xpathOnNewLetter))).text
        this.draftSubject = driver.findElement(By.xpath(String.format(DRAFT_SUBJECT, xpathOnNewLetter))).text
        this.draftMessage = driver.findElement(By.xpath(String.format(DRAFT_MESSAGE, xpathOnNewLetter))).text
    }

    fun sendLetterFromDrafts(): SentPage {
        if (xpathOnNewLetter == null)
            getLetterInfoInDrafts()
        driver.findElement(By.xpath(xpathOnNewLetter!!)).click()
        driver.waitForElementPresent(sendButton)
        driver.clickStale(sendButton)
        val sentPage = SentPage(driver)
        sentPage.setSendLetterXpath(idOfSentLetter)
        pressSendLettersButton()
        return sentPage
    }

    fun setId(newId: String) {
        this.xpathOnNewLetter = String.format("//div[contains(@data-id,'%s')]", newId)
    }

    fun getDraftGetter(): String? {
        if (draftGetter == null)
            getLetterInfoInDrafts()
        return draftGetter
    }

    fun getDraftSubject(): String? {
        if (draftSubject == null)
            getLetterInfoInDrafts()
        return draftSubject
    }

    fun getDraftMessage(): String? {
        if (draftMessage == null)
            getLetterInfoInDrafts()
        return draftMessage
    }
}
