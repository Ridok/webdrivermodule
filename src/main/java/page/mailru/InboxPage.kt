package page.mailru

import decorator.WebDriverDecorator
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import page.Urls
import util.constant.MAIL_RU_TITLE

class InboxPage(driver: WebDriverDecorator) : EMailPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(InboxPage::class.java)
    @FindBy(xpath = "//*[@id=\"mailbox:error\"]")
    private val error: WebElement? = null

    val isItInboxPageTitle: Boolean
        get() {
            driver.waitForPageLoad()
            log.info("Checking inbox page title")
            return driver.title.contains(MAIL_RU_TITLE)
        }

    val isLoginSuccessful: Boolean
        get() {
            driver.waitForPageLoad()
            log.info("Checking is login successful")
            return error!!.isDisplayed
        }

    init {
        this.url = Urls.INBOX.url
        PageFactory.initElements(this.driver, this)
    }
}
