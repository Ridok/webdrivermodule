package page.mailru

import decorator.WebDriverDecorator
import org.openqa.selenium.By
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import page.Urls.SENT

class SentPage(driver: WebDriverDecorator) : EMailPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(SentPage::class.java)
    private var sentMail: String? = null
    private var sentSubject: String? = null
    private var sentMessage: String? = null
    private var sendLetterXpath: String? = null

    val isLetterInSentFolder: Boolean
        get() {
            log.info("Checking is letter in sent folder")
            return driver.findElements(By.xpath(sendLetterXpath!!)).isNotEmpty()
        }

    val sentGetter: String?
        get() {
            if (sentMail == null)
                getInfo()
            return sentMail
        }

    init {
        this.url = SENT.url
        PageFactory.initElements(this.driver, this)
    }

    fun setSendLetterXpath(id: String) {
        this.sendLetterXpath = String.format("//div[contains(@data-id,'%s')]", id)
    }

    fun getSentSubject(): String? {
        if (sentSubject == null)
            getInfo()
        return sentSubject
    }

    fun getSentMessage(): String? {
        if (sentMessage == null)
            getInfo()
        return sentMessage
    }

    private fun getInfo() {
        sentMail = driver.findElement(By.xpath(String.format("%s//div[@class='b-datalist__item__addr']", sendLetterXpath))).text
        sentSubject = driver.findElement(By.xpath(String.format("%s//div[@class='b-datalist__item__subj']", sendLetterXpath))).text
        sentMessage = driver.findElement(By.xpath(String.format("%s//span[@class='b-datalist__item__subj__snippet']", sendLetterXpath))).text
    }
}
