package page.mailru

import decorator.WebDriverDecorator
import jdk.nashorn.internal.runtime.regexp.joni.Config.log
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import page.CustomPage

import page.Urls.EMAIL


open class EMailPage(driver: WebDriverDecorator) : CustomPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(EMailPage::class.java)
    @FindBy(xpath = "//*[@id=\"b-nav_folders\"]/div/div[2]")
    private val sendLettersButton: WebElement? = null
    @FindBy(xpath = "//*[@id=\"b-nav_folders\"]/div/div[3]")
    private val draftsButton: WebElement? = null
    @FindBy(xpath = "//*[@id=\"PH_logoutLink\"]")
    private val logoutButton: WebElement? = null
    @FindBy(xpath = "//span[@class='b-toolbar__btn__text b-toolbar__btn__text_pad']")
    private val newLetterButton: WebElement? = null
    @FindBy(xpath = "//*[@id=\"w-portal-footer\"]/table/tbody/tr/td[1]/div/a[1]/span")
    private val footer: WebElement? = null

    val isFooterOnPage: Boolean
        get() {
            log.info("Checking element in page footer")
            return footer!!.isDisplayed
        }

    val isLogOutSuccessful: Boolean
        get() {
            log.info("Checking is logout successful")
            return driver.currentUrl.contains("https://mail.ru")
        }

    init {
        this.url = EMAIL.url
        PageFactory.initElements(this.driver, this)
    }

    internal fun pressSendLettersButton() {
        sendLettersButton!!.click()
    }

    internal fun pressDraftsButton() {
        driver.clickStale(draftsButton)
    }

    fun writeNewLetter(): NewLetterPage {
        log.info("Writing new letter.")
        pressNewLetterButton()
        val page = NewLetterPage(driver)
        page.setUrl(driver.currentUrl)
        return page
    }

    fun logOut(): MailRuPage {
        pressLogoutButton()
        return MailRuPage(driver)
    }

    private fun pressLogoutButton() {
        logoutButton!!.click()
    }

    private fun pressNewLetterButton() {
        newLetterButton!!.click()
    }
}
