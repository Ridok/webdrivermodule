package page.mailru

import decorator.WebDriverDecorator
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import page.CustomPage
import page.Urls.MAIL_RU

class MailRuPage(driver: WebDriverDecorator) : CustomPage(driver) {
    private val log: Logger = LoggerFactory.getLogger(MailRuPage::class.java)
    @FindBy(xpath = "//*[@id=\"mailbox:login\"]")
    private val usernameField: WebElement? = null

    @FindBy(xpath = "//*[@id=\"mailbox:password\"]")
    private val passwordField: WebElement? = null

    @FindBy(xpath = "//*[@id=\"mailbox:submit\"]/input")
    private val enterButton: WebElement? = null

    init {
        this.url = MAIL_RU.url
        PageFactory.initElements(this.driver, this)
    }

    fun signInWithCredentials(username: String, password: String): InboxPage {
        log.info("Signing in with credentials")
        Actions(driver).sendKeys(usernameField!!, username)
            .build()
            .perform()
        Actions(driver).sendKeys(passwordField!!, password)
            .build()
            .perform()
        Actions(driver).click(enterButton!!)
            .build()
            .perform()
        val inbox = InboxPage(driver)
        waitForCondition(InboxPage::isLoginSuccessful, inbox)
        return inbox
    }

    override fun openUrl(): MailRuPage {
        return super.openUrl() as MailRuPage
    }
}
